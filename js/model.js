/**
 * Created with JetBrains WebStorm.
 * User: ruslan
 * Date: 24.01.16
 * Time: 0:17
 * To change this template use File | Settings | File Templates.
 */

(function (root, factory) {
    root.returnExports = factory(root._);
}(this, function (_) {
    Model = function(){
        Date.prototype.daysInMonth= function(){
            var d= new Date(this.getFullYear(), this.getMonth()+1, 0);
            return d.getDate();
        }
        var date = new Date();
        var selectedDate = new Date();
        var min = new Date(1900, 0, 1);
        var max = new Date(2100, 0, 1);
        this.monthNames = ['янв', 'фев', 'мар', 'апр', 'май', 'июн', 'июл', 'авг', 'сен', 'окт', 'ноя', 'дек'];
        this.dayNames = [{name:'Пн'}, {name:'Вт'}, {name:'Ср'}, {name:'Чт'}, {name:'Пт'}, {name:'Сб', weekend:true}, {name:'Вс', weekend:true}];
        this.years = function(){
            var y = [];
            for(var i=min.getFullYear(); i<=max.getFullYear(); i++){
                y.push(''+i)
            }
            return y;
        }
        this.months = function(){
            var m = [];
            for(var i=0; i<this.monthNames.length; i++){
                var daysInMonth = new Date(date.getFullYear(), i, 1).daysInMonth();
                var first = new Date(date.getFullYear(), i, daysInMonth);
                var second = new Date(date.getFullYear(), i, 1);
                if(first.getTime() >= min.getTime() && second.getTime() <= max.getTime()){
                    m.push(this.monthNames[i]);
                }
            }
            return m;
        }
        this.canPrev = function(){
            var year = date.getFullYear();
            var month = date.getMonth();
            if((month-1)<0){
                year--;
                month = 11;
            }else month--;
            var daysInMonth = new Date(year, month, 1).daysInMonth();
            var n = new Date(year, month, daysInMonth);
            return n.getTime() >= min.getTime();
        }
        this.canNext = function(){
            var year = date.getFullYear();
            var month = date.getMonth();
            if((month+1)>11){
                month = 0;
                year++
            }else month++;
            var n = new Date(year, month, 1);
            return n.getTime() <= max.getTime();
        }
        this.setMin = function(d){
            if(d.getTime() > max.getTime()){
                throw new Error("Wrong min date.");
            }else{
              min = d;
              if(date.getTime()<min.getTime()){
                date = new Date(min.getFullYear(), min.getMonth(), min.getDate());
                selectedDate = new Date(min.getFullYear(), min.getMonth(), min.getDate());
              }
            }
        }
        this.setMax = function(d){
            if(d.getTime() < min.getTime()){
                throw new Error("Wrong max date.");
            }else{
              max = d;
              if(date.getTime()>max.getTime()){
                date = new Date(max.getFullYear(), max.getMonth(), max.getDate());
                selectedDate = new Date(max.getFullYear(), max.getMonth(), max.getDate());
              }
            }
        }
        this.setSelectedDay = function(day){
            selectedDate = new Date(date.getFullYear(), date.getMonth(),day);
        }
        this.getSelectedDate = function(){
            return selectedDate;
        }
        this.curMonthName = function(){
            return this.monthNames[date.getMonth()];
        }
        this.curYear = function(){
            return date.getFullYear();
        }
        this.setYear = function(year){
            var newDate = new Date(year, date.getMonth(), date.getDate());
            if(newDate.getTime()<min.getTime()){
                date.setFullYear(min.getFullYear());
                date.setMonth(min.getMonth());
                date.setDate(min.getDate());
            }else if(newDate.getTime()>max.getTime()){
                date.setFullYear(max.getFullYear());
                date.setMonth(max.getMonth());
                date.setDate(max.getDate());
            }else
            date.setFullYear(year);
        }
        this.setMontByName = function(month){
            var i = _.indexOf(this.monthNames, month);
            if(i>=0){
                var newDate = new Date(date.getFullYear(), i, date.getDate());
                if(newDate.getTime()<min.getTime()){
                    date.setFullYear(min.getFullYear());
                    date.setMonth(min.getMonth());
                    date.setDate(min.getDate());
                }else if(newDate.getTime()>max.getTime()){
                    date.setFullYear(max.getFullYear());
                    date.setMonth(max.getMonth());
                    date.setDate(max.getDate());
                }else
                    date.setMonth(i);
            }

        }
        this.increaseMonth = function(){
            if(!this.canNext()) return;
            var mon = date.getMonth()+1;
            if(mon>=this.monthNames.length){
                mon = 0;
                this.increaseYear();
            }
            date.setMonth(mon);
        }
        this.decreaseMonth = function(){
            if(!this.canPrev()) return;
            var mon = date.getMonth()-1;
            if(mon<0){
                mon = this.monthNames.length-1;
                this.decreaseYear();
            }
            date.setMonth(mon);
        }
        this.increaseYear = function(){
            date.setFullYear(date.getFullYear()+1);
        }
        this.decreaseYear = function(){
            date.setFullYear(date.getFullYear()-1);
        }
        this.getWeeks = function(){
            var daysInMonth = date.daysInMonth();
            var cols = this.dayNames.length;

            var firstDayOfWeek = new Date(date.getFullYear(), date.getMonth(), 1).getDay()-1;
            if(firstDayOfWeek<0) firstDayOfWeek = 6;
            var rows = Math.ceil(((firstDayOfWeek+daysInMonth)*1.0)/cols);
            weeks = [];
            var days = 0;
            for(i=0; i<rows; i++){
                for(j=0; j<cols; j++){
                    if(j==0) weeks[i] = [];
                    if(i==0 && j<firstDayOfWeek) weeks[i].push({});
                    else if(days<daysInMonth){
                        var day = {name: ++days}
                        if(j>=5) day.weekend = true;
                        var curDate = new Date(date.getFullYear(), date.getMonth(), days);
                        if(curDate.getTime()<min.getTime())
                            day.disabled = true;
                        if(curDate.getTime()>max.getTime())
                            day.disabled = true;
                        weeks[i].push(day);
                    }else weeks[i].push({});
                }

            }
            return weeks;

        }
    }
    return Model;
}));