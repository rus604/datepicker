/**
 * Created with JetBrains WebStorm.
 * User: ruslan
 * Date: 24.01.16
 * Time: 13:25
 * To change this template use File | Settings | File Templates.
 */

(function (root, factory) {
    root.returnExports = factory();
}(this, function () {
    Number.prototype.fit = function(size) {
      var s = String(this);
      while (s.length < (size || 2)) {s = "0" + s;}
      return s;
    }
    Formatter = function(){
        var monthNames = ['Января', 'Февраля', 'Марта', 'Апреля', 'Мая', 'Июня', 'Июля', 'Августа', 'Сентября', 'Октября', 'Ноября', 'Декабря'];
        this.format = function(date, f){
            var words = f.split(/[\/.-]+/)
            var separators = f.match(/[\/.-]+/g);

            var momentFormat = "";
            var separatorCnt = 0;
            if(words.length){
                for(var i=0; i<words.length; i++){
                    if(words[i] == 'dd') momentFormat+=date.getDate().fit(2);
                    else if(words[i] == 'MM') momentFormat+=(date.getMonth()+1).fit(2);
                    else if(words[i] == 'MMM') {
                        momentFormat+=monthNames[date.getMonth()];
                    }
                    else if(words[i] == 'yy') momentFormat+=date.getFullYear().fit(4).substr(2,2);
                    else if(words[i] == 'yyyy') momentFormat+=date.getFullYear().fit(4);
                    else {
                        throw new SyntaxError("Date format error: '"+words[i]+"'");
                    }

                    if(separators && separatorCnt<separators.length){
                        momentFormat+=separators[separatorCnt++];
                    }
                }
            }

            return momentFormat;
        }
    }
    return Formatter;
}));