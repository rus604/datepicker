/**
 * Created with JetBrains WebStorm.
 * User: ruslan
 * Date: 24.01.16
 * Time: 0:00
 * To change this template use File | Settings | File Templates.
 */

(function (root, factory) {
    root.returnExports = factory(root.Model, root.Formatter, root._);
}(this, function (Model, Formatter, _) {

    Datepicker = function(element, opts){
        var defoptions = {
            format: "dd/MM/yyyy"
        }
        var options = _.clone(defoptions);
        $this = this;
        var position = {top: 0, left: 0, height: 0, width: 0}
        var model = new Model();
        var formatter = new Formatter();
        var view = document.createElement('div');
        var template = "\
        <div class='dpHeader'>\
          <a class='prevMonth <%= canPrev()?\"\":\"disabled\" %>' > <span> < </span> </a>\
          <div class='dpTitle'>\
            <select class='monthSelector'><% _.each(months(), function(name){%> <option <%= curMonthName()==name?'selected':'' %> > <%= name %> </option> <%}); %>\
            </select>\
            <select class='yearSelector'> <% _.each(years(), function(year){%> <option <%= curYear()==year?'selected':'' %>  > <%= year %> </option> <%}); %>\
            </select>\
          </div>\
          <a class='nextMonth <%= canNext()?\"\":\"disabled\" %>' > <span> > </span> </a>\
        </div>\
        <table class='dpTable'>\
            <thead>\
              <tr><% _.each(dayNames, function(day){%> <th> <span <%= day.weekend?'class=\"weekend\"':\"\"%> ><%= day.name %></span> </th> <%}); %> </tr>\
            </thead>\
            <% _.each(getWeeks(), function(week){%> <tr> <%\
                _.each(week, function(day){ %> \
                    <td><% if(day.name){ %> \
                      <% if(day.disabled){ %>\
                        <span class='disabled <%= day.weekend?\"weekend\":\"\"%>'> <%= day.name %> </span>\
                      <% } else { %> \
                        <a class='dayButton <%= day.weekend?\"weekend\":\"\"%>' href='#'> <%= day.name %> </a> \
                      <% } %> \
                    <% } %> </td> \
                <%}); \
            %> </tr> <%}); %>\
        </table>";

        function destroy(){
            view.parentNode.removeChild(view);
            window.removeEventListener("click", htmlOnClick, true);
        }

        function render(){
            view.className = "dpView";
            view.style.top = (position.top+position.height)+"px";
            view.style.left = position.left+"px";
            view.innerHTML = _.template(template, model);

            _.each(view.getElementsByClassName("monthSelector"), function(el){
                el.onchange = function(e){
                    console.log('change:'+ e.target.value);
                    model.setMontByName(e.target.value);
                    render();
                }
            });

            _.each(view.getElementsByClassName("yearSelector"), function(el){
                el.onchange = function(e){
                    console.log('change:'+ e.target.value);
                    model.setYear(parseInt(e.target.value));
                    render();
                }
            });

            _.each(view.getElementsByClassName("dayButton"), function(el){
                el.onclick = function(e){
                    console.log('change:'+ e.target.innerHTML);
                    model.setSelectedDay(parseInt(e.target.innerHTML));
                    element.value = formatter.format(model.getSelectedDate(), options.format);
                    if(options.backInput) {
                        options.backInput.value = formatter.format(model.getSelectedDate(), options.backFormat?options.backFormat:options.format);
                    }
                    if(options.listener){
                        options.listener(model.getSelectedDate());
                    }
                    destroy();
                }
            });

            _.each(view.getElementsByClassName("prevMonth"), function(el){
                el.onclick = function(e){
                    console.log('change: prevMonth');
                    model.decreaseMonth();
                    render();
                }
            });
            _.each(view.getElementsByClassName("nextMonth"), function(el){
                el.onclick = function(e){
                    console.log('change: prevMonth');
                    model.increaseMonth();
                    render();
                }
            });
        }




        function htmlOnClick(event){
            var inMe = false;
            for (var element = event.target; element; element = element.parentNode) {
                if (element === view) {
                    inMe = true;
                    break;
                }
            }
            if(!inMe)  {
                destroy();
            }
        }

        element.onclick = function(){
            position = this.getBoundingClientRect();
            render();
            this.parentNode.appendChild(view);
            window.addEventListener("click", htmlOnClick, true);
        }

        function init(){
            _.each(opts, function(value, key){
                options[key] = value;
            });
            if(options.min){
                model.setMin(options.min);
            }
            if(options.max){
                model.setMax(options.max);
            }
        }

        init();

    }
    return Datepicker;
}));
